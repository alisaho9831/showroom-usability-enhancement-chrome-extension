const elem = document.getElementsByClassName("room-video-wrapper")[0];
const btn_ul = document.getElementsByClassName("st-activate__list")[0];

//init
(function () {
  addFullScreenButton();
})();

// methods
function addFullScreenButton() {
  const button_list = document.createElement("li");
  button_list.className = "st-activate__item";
  btn_ul.appendChild(button_list);

  const countAll = document.querySelectorAll(".st-activate__item").length;
  console.log(countAll);

  const btn_li =
    document.getElementsByClassName("st-activate__item")[countAll - 1];

  // 1. Create the button
  const button = document.createElement("button");
  button.innerHTML = " Full ";
  button.className = "st-activate__button fbtn";

  // 2. Append somewhere
  btn_li.appendChild(button);

  button.addEventListener("click", function () {
    openFullscreen();
  });

  elem.addEventListener("click", function () {
    if (this === document.fullscreenElement) {
      closeFullscreen();
    }
  });
}

function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) {
    /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    /* IE11 */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) {
    /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE11 */
    document.msExitFullscreen();
  }
}
